package edu.westga.android.brogers.pedometer;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * MainActivity - Counts the amount of steps the user takes, separated by
 * session and overall steps taken since the last reboot.
 * 
 * @author Brian Rogers
 * @version September 16th, 2014
 */
public class MainActivity extends Activity implements SensorEventListener {
	private static final String LOG_TAG = "PedometerExample";

	private SensorManager sensorManager;
	private TextView sessionStepsLabel;
	private TextView sessionStepsCount;
	private TextView overallStepsLabel;
	private TextView overallStepsCount;
	private TextView userPrompt;

	private boolean active;
	private int initialStepCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		initialStepCount = -1;

		final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
		stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
			@Override
			public void onLayoutInflated(WatchViewStub stub) {
				userPrompt = (TextView) findViewById(R.id.userPrompt);
				sessionStepsLabel = (TextView) findViewById(R.id.sessionLabel);
				sessionStepsCount = (TextView) findViewById(R.id.sessionStepCount);
				overallStepsLabel = (TextView) findViewById(R.id.overallLabel);
				overallStepsCount = (TextView) findViewById(R.id.overallStepCount);
			}
		});

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

	@Override
	/**
	 * Allows for UI updates and registers a listener on the Step Counter.
	 */
	protected void onResume() {
		super.onResume();

		active = true;

		Sensor countSensor = sensorManager
				.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);

		if (countSensor != null) {
			sensorManager.registerListener(this, countSensor,
					SensorManager.SENSOR_DELAY_FASTEST);
		} else {
			String error = getString(R.string.error_sensor_unavailable);

			Log.e(LOG_TAG, error);
			Toast.makeText(this, error, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	/**
	 * Prevent UI updates while the activity is not active.
	 */
	protected void onPause() {
		super.onPause();

		active = false;

		// Uncomment this to prevent counting steps in the background.
		// sensorManager.unregisterListener(this);
	}

	@Override
	/**
	 * Unregisters the listener on the Step Sensor.
	 */
	protected void onStop() {
		super.onStop();

		sensorManager.unregisterListener(this);
	}

	@Override
	/**
	 * Fired when the Step Sensor detects a step taken. Signals the GUI to
	 * update the session-specific and overall step counts.
	 * 
	 * NOTE: Even when set to the fastest update speed available, the Step 
	 * Sensor (at least in the Samsung Gear Live) appears to necessitate priming
	 * it with constant motion for ~10 seconds before the event will begin to 
	 * fire. Some testing suggests that Fit (the preinstalled pedometer app) is
	 * likewise affected. I was unable to find any documentation relating to 
	 * this issue, so I would conjecture one of these possibilities:
	 *   - This may be an intentional design decision intended to maximize
	 *     battery life (at the cost of accuracy, assuming constant motion 
	 *     throughout use).
	 *   - This may be a bug either in Samsung's firmware implementation of the
	 *     Step Sensor, or within the Android API (support started with KitKat).
	 */
	public void onSensorChanged(SensorEvent event) {
		Log.i(LOG_TAG, "Step event fired.");

		int stepCount = (int) event.values[0];

		if (initialStepCount == -1) {
			initialStepCount = stepCount - 1;

			Log.i(LOG_TAG, "Initial step count: " + initialStepCount);
		}

		int sessionStepCount = stepCount - initialStepCount;

		Log.i(LOG_TAG, "Current session step count: " + sessionStepCount);
		Log.i(LOG_TAG, "Overall step count: " + stepCount);

		if (active) {
			Locale userLocale = getResources().getConfiguration().locale;
			NumberFormat formatter = NumberFormat.getNumberInstance(userLocale);

			if (sessionStepsCount != null && overallStepsCount != null) {
				String formattedSessionCount = formatter
						.format(sessionStepCount);
				String formattedOverallCount = formatter.format(stepCount);

				showData();

				sessionStepsCount.setText(formattedSessionCount);
				overallStepsCount.setText(formattedOverallCount);
			}
		}
	}

	private void showData() {
		userPrompt.setVisibility(View.INVISIBLE);
		sessionStepsLabel.setVisibility(View.VISIBLE);
		sessionStepsCount.setVisibility(View.VISIBLE);
		overallStepsLabel.setVisibility(View.VISIBLE);
		overallStepsCount.setVisibility(View.VISIBLE);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// Not implemented in this example.
	}
}
